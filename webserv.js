var http = require('http');

http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	var table = [10, 12, 22],
		response = table.indexOf(13).toString();
		// indexOf zwraca wartość -1 gdy nie znajdzie elementu
	res.end(response);
}).listen(1337, '127.0.0.1');

console.log('Server running at http://127.0.0.1:1337/');